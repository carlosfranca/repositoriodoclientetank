/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

import POJO.Usuario;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import javax.ejb.Stateless;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import org.hibernate.Session;
import org.hibernate.Transaction;
import system.HibernateUtil;

/**
 *
 * @author carlos
 */
@Named
@RequestScoped
public class FrequenciaHelper implements Serializable {

    String stracademia = "";
    String straula = "";
    String strmes = "";
    String strano = "";
    transient HtmlOutputText mesFrequencia = new HtmlOutputText();
    transient HtmlOutputText anoFrequencia = new HtmlOutputText();

    public FrequenciaHelper() {
        Calendar c = new GregorianCalendar();
        strmes = String.valueOf(c.get(Calendar.MONTH));
        strano = String.valueOf(c.get(Calendar.YEAR));
        straula = "1";
    }

    /**
     * Contagem de frequencias de todos os usuarios por mes indicado e por
     * turma. Usado em addAprovacao em admin.xhtml
     */
    public List<Object[]> getUsuariosFreqMap() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            tr = session.beginTransaction();
            Usuario usuario = new Usuario();
            String dt1 = strano + "-" + strmes + "-01";
            String dt2 = strano + "-" + strmes + "-";
            int mes = Integer.valueOf(strmes).intValue();
            switch (mes) {
                case 1:
                    dt2 += "31";
                    break;
                case 2:
                    dt2 += "28";
                    break;
                case 3:
                    dt2 += "31";
                    break;
                case 4:
                    dt2 += "30";
                    break;
                case 5:
                    dt2 += "31";
                    break;
                case 6:
                    dt2 += "30";
                    break;
                case 7:
                    dt2 += "31";
                    break;
                case 8:
                    dt2 += "31";
                    break;
                case 9:
                    dt2 += "30";
                    break;
                case 10:
                    dt2 += "31";
                    break;
                case 11:
                    dt2 += "30";
                    break;
                case 12:
                    dt2 += "31";
                    break;
            }
            List<Object[]> lista = session.createNativeQuery(
                    "SELECT u.nome, sum(c.confirma) contagem FROM tf.checkin c inner join tf.usuario u on u.email = c.usuario_email inner join tf.aula a on a.idaula = c.aula_idaula inner join tf.categoriaaula ct on ct.idcategoriaaula = a.categoriaaula_idcategoriaaula where ct.idcategoriaaula = '" + straula + "' and c.create_time between '" + dt1 + "' and '" + dt2 + "' group by c.usuario_email order by contagem desc")
                    .getResultList();
            return lista;
        } catch (Exception e) {
            addMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
        List<Object[]> lista = new ArrayList<>();
        return lista;
    }

    /**
     * Lista Aulas da última semana disponíveis para aprovação de checkins.
     * Regra: a.data dos 6 dias anteriores. e existe checkin para ser aprovado.
     * Onde:Tela aprovação.xhtml.
     */
    public List<Object[]> getUsuariosInativos() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            tr = session.beginTransaction();
            Calendar a = Calendar.getInstance();
            a.setTime(new Date());//data maior
            a.add(Calendar.DAY_OF_MONTH, -15);
            java.sql.Date sqlDate = new java.sql.Date(a.getTimeInMillis());
            List<Object[]> lista = session.createNativeQuery(
                    "SELECT user.nome from tf.usuario user where user.email not in(SELECT u.email FROM tf.checkin c inner join tf.usuario u on u.email = c.usuario_email inner join tf.aula a on a.idaula = c.aula_idaula inner join tf.categoriaaula ct on ct.idcategoriaaula = a.categoriaaula_idcategoriaaula where ct.idcategoriaaula = '" + straula + "' and c.create_time >= '" + sqlDate + "' and c.confirma='1')")
                    .getResultList();
            if (!lista.isEmpty()) {
                return lista;
            }
        } catch (Exception e) {
            //tr.rollback();
            addMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return new ArrayList<>();
    }

    /**
     * Contagem de frequencias do usuario logado por mes indicado e por turma.
     * Usado em addAprovacao em arealogada.xhtml botao frequencia
     */
    public List<Object[]> getFreqPorMesUsuarioLogado() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession sessao = (HttpSession) facesContext.getExternalContext().getSession(true);
        String idUsuarioLogado = "" + (String) sessao.getAttribute("idUserLogged");
        try {
            tr = session.beginTransaction();
            Calendar a = Calendar.getInstance();
            a.setTime(new Date());//data maior
            a.add(Calendar.DAY_OF_MONTH, -365);
            java.sql.Date sqlDate = new java.sql.Date(a.getTimeInMillis());
            List<Object[]> listaAula = session.createNativeQuery("select first.month, first.year, sum(first.contagem) from (SELECT MONTH(cc.create_time) as month, YEAR(cc.create_time) as year, sum(cc.qtd) as contagem FROM tf.checkincompeticao cc inner join tf.usuario u on u.email = cc.usuario_email and cc.create_time >= '2016-08-01' and u.email='carlosalan2@hotmail.com' group by MONTH(cc.create_time) union all SELECT MONTH(c.create_time) mes,  YEAR(c.create_time) ano, count(c.confirma) contagem FROM tf.checkin c inner join tf.usuario u on u.email = c.usuario_email inner join tf.aula a on a.idaula = c.aula_idaula inner join tf.categoriaaula ct on ct.idcategoriaaula = a.categoriaaula_idcategoriaaula where ct.idcategoriaaula = 1 and c.create_time >= '2016-01-01' and c.confirma = 1 and u.email='carlosalan2@hotmail.com' group by MONTH(c.create_time) union all SELECT MONTH(cs.create_time), YEAR(cs.create_time), sum(cs.qtd) contagem FROM tf.checkinseminario cs inner join tf.usuario u on u.email = cs.usuario_email and cs.create_time >= '2016-08-01' and u.email='carlosalan2@hotmail.com' group by MONTH(cs.create_time) ) first group by first.month, first.year")
                    //"SELECT MONTH(c.create_time), YEAR(c.create_time), count(c.confirma) contagem FROM tf.checkin c inner join tf.usuario u on u.email = c.usuario_email inner join tf.aula a on a.idaula = c.aula_idaula inner join tf.categoriaaula ct on ct.idcategoriaaula = a.categoriaaula_idcategoriaaula where ct.idcategoriaaula = '" + straula + "' and c.create_time >= '" + sqlDate + "' and c.confirma = 1 and u.email='" + idUsuarioLogado + "' group by MONTH(c.create_time) order by c.create_time desc")
                    .getResultList();
            return listaAula;
        } catch (Exception e) {
            addMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
        List<Object[]> lista = new ArrayList<>();
        return lista;
    }

    /**
     * Contagem de frequências em aula/mes do usuario logado Usado em relatorio
     * de frequência em arealogada.xhtml botao frequencia
     */
    public String getFreqAulaPorMes() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession sessao = (HttpSession) facesContext.getExternalContext().getSession(true);
        String idUsuarioLogado = "" + (String) sessao.getAttribute("idUserLogged");
        try {
            tr = session.beginTransaction();
            List<Object> listaContagem = session.createNativeQuery(
                    "SELECT count(c.confirma) contagem FROM tf.checkin c inner join tf.usuario u on u.email = c.usuario_email inner join tf.aula a on a.idaula = c.aula_idaula inner join tf.categoriaaula ct on ct.idcategoriaaula = a.categoriaaula_idcategoriaaula where ct.idcategoriaaula = '" + straula + "' and month(c.create_time) = '" + mesFrequencia.getValue().toString() + "' and year(c.create_time) = '" + anoFrequencia.getValue().toString() + "'and c.confirma = 1 and u.email='" + idUsuarioLogado + "' ")
                    .getResultList();
            return listaContagem.get(0).toString();
        } catch (Exception e) {
            addMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
        return "0";
    }

    /**
     * Contagem de frequências em seminario/mes do usuario logado Usado em
     * relatorio de frequência em arealogada.xhtml botao frequencia
     */
    public String getFreqSeminarioPorMes() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession sessao = (HttpSession) facesContext.getExternalContext().getSession(true);
        String idUsuarioLogado = "" + (String) sessao.getAttribute("idUserLogged");
        try {
            tr = session.beginTransaction();
            List<Object> listaContagem = session.createNativeQuery(
                    "SELECT sum(c.qtd) contagem FROM tf.checkinseminario c inner join tf.usuario u on u.email = c.usuario_email where month(c.create_time) = '" + mesFrequencia.getValue().toString() + "' and year(c.create_time) = '" + anoFrequencia.getValue().toString() + "'and u.email='" + idUsuarioLogado + "' ")
                    .getResultList();
            if (listaContagem.get(0) != null) {
                return listaContagem.get(0).toString();
            }

        } catch (Exception e) {
            addMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
        return "0";
    }

    /**
     * Contagem de frequências em competição/mes do usuario logado Usado em
     * relatorio de frequência em arealogada.xhtml botao frequencia
     */
    public String getFreqCompeticaoPorMes() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession sessao = (HttpSession) facesContext.getExternalContext().getSession(true);
        String idUsuarioLogado = "" + (String) sessao.getAttribute("idUserLogged");
        try {
            tr = session.beginTransaction();
            List<Object> listaContagem = session.createNativeQuery(
                    "SELECT sum(c.qtd) contagem FROM tf.checkincompeticao c inner join tf.usuario u on u.email = c.usuario_email where month(c.create_time) = '" + mesFrequencia.getValue().toString() + "' and year(c.create_time) = '" + anoFrequencia.getValue().toString() + "'and u.email='" + idUsuarioLogado + "' ")
                    .getResultList();
            return listaContagem.get(0).toString();
        } catch (Exception e) {
            addMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
        return "0";
    }

    /*
    public List<Object[]> getFreqPorMesUsuarioLogado() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession sessao = (HttpSession) facesContext.getExternalContext().getSession(true);
        String idUsuarioLogado = "" + (String) sessao.getAttribute("idUserLogged");
        try {
            tr = session.beginTransaction();
            Calendar a = Calendar.getInstance();
            a.setTime(new Date());//data maior
            a.add(Calendar.DAY_OF_MONTH, -365);
            java.sql.Date sqlDate = new java.sql.Date(a.getTimeInMillis());
            List<Object[]> lista = session.createNativeQuery(
                    "SELECT MONTH(c.create_time), YEAR(c.create_time), count(c.confirma) contagem FROM tf.checkin c inner join tf.usuario u on u.email = c.usuario_email inner join tf.aula a on a.idaula = c.aula_idaula inner join tf.categoriaaula ct on ct.idcategoriaaula = a.categoriaaula_idcategoriaaula where ct.idcategoriaaula = '" + straula + "' and c.create_time >= '" + sqlDate + "' and c.confirma = 1 and u.email='" + idUsuarioLogado + "' group by MONTH(c.create_time) order by c.create_time desc")
                    .getResultList();
            return lista;
        } catch (Exception e) {
            addMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
        List<Object[]> lista = new ArrayList<>();
        return lista;
    }
     */
    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public String getStracademia() {
        return stracademia;
    }

    public void setStracademia(String stracademia) {
        this.stracademia = stracademia;
    }

    public String getStraula() {
        return straula;
    }

    public void setStraula(String straula) {
        this.straula = straula;
    }

    public String getStrmes() {
        return strmes;
    }

    public void setStrmes(String strmes) {
        this.strmes = strmes;
    }

    public String getStrano() {
        return strano;
    }

    public void setStrano(String strano) {
        this.strano = strano;
    }

    public HtmlOutputText getMesFrequencia() {
        return mesFrequencia;
    }

    public void setMesFrequencia(HtmlOutputText mesFrequencia) {
        this.mesFrequencia = mesFrequencia;
    }

    public HtmlOutputText getAnoFrequencia() {
        return anoFrequencia;
    }

    public void setAnoFrequencia(HtmlOutputText anoFrequencia) {
        this.anoFrequencia = anoFrequencia;
    }

}
