/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

import POJO.Checkincompeticao;
import POJO.Competicao;

import POJO.Usuario;


import java.io.Serializable;

import java.util.ArrayList;
import java.util.Calendar;

import java.util.Date;

import java.util.List;


import javax.faces.application.FacesMessage;

import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import javax.servlet.http.HttpSession;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.primefaces.event.SelectEvent;

import org.primefaces.mobile.event.SwipeEvent;
import system.HibernateUtil;

/**
 *
 * @author carlos
 */
@Named
@ViewScoped
public class CompeticoesHelper implements Serializable {

    private Competicao competicaoSelecionada = new Competicao();
    private Date date1;
    private String strLocal, strNome, strSite, strTipoCheckin = "";

    public CompeticoesHelper() {
    }

    /**
     * Faz Checkin ou remove checkin em uma competição, conforme localiza ou não
     * checkin para o usuário logado e aula. Usado em arealogada.xhtml
     */
    public void fazCheckin() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Boolean validaPag = false;
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession sessao = (HttpSession) facesContext.getExternalContext().getSession(true);
        String idUsuarioLogado = "" + (String) sessao.getAttribute("idUserLogged");
        try {
            tr = session.beginTransaction();
            java.util.Date utilDate = new java.util.Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
            List<Checkincompeticao> listaCheckin = session.createQuery(
                    "select c  "
                    + "from Checkincompeticao c where c.competicao.data ='" + getCompeticaoSelecionada().getData() + "' and c.usuario.email = '" + idUsuarioLogado + "'", Checkincompeticao.class
            )
                    .getResultList();
            Checkincompeticao checkin = new Checkincompeticao();
            if (listaCheckin.isEmpty()) {
                List<Usuario> listaUsuario = session.createQuery(
                        "select u  "
                        + "from Usuario u where u.email = '" + idUsuarioLogado + "'", Usuario.class
                )
                        .getResultList();
                checkin.setUsuario(listaUsuario.get(0));
                checkin.setCreateTime(utilDate);
                checkin.setCompeticao(competicaoSelecionada);
                checkin.setTipo(strTipoCheckin);
                session.saveOrUpdate(checkin);
                session.merge(checkin);
                tr.commit();
                addMessage("Checkin recebido");
                competicaoSelecionada = new Competicao();
            } else {
                checkin = (Checkincompeticao) listaCheckin.get(0);
                session.delete(checkin);
                session.saveOrUpdate(competicaoSelecionada);
                session.flush();
                tr.commit();
                addMessage("Checkin retirado");
            }
        } catch (Exception e) {
            if (tr != null && tr.isActive()) {
                try {
// Second try catch as the rollback could fail as well
                    tr.rollback();
                } catch (HibernateException e1) {
                    System.out.println("Erro no rollback do transaction");
                }
// throw again the first exception
                throw e;
            }
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public void onDateSelect(SelectEvent event) {
    }

    /**
     * Listas as próximas Competicoes a partir de hoje. Usado em
     * arealogada.xhtml
     */
    public List<Competicao> getListaProximasCompeticoes() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            tr = session.beginTransaction();
            Calendar a = Calendar.getInstance();
            a.setTime(new Date());//data maior
            a.add(Calendar.DAY_OF_MONTH, -1);
            java.sql.Date sqlDate = new java.sql.Date(a.getTimeInMillis());
            List<Competicao> lista = session.createQuery(
                    "select c  "
                    + "from Competicao c "
                    + "where c.data >='" + sqlDate + "' order by  c.data", Competicao.class
            )
                    .getResultList();
            if (!lista.isEmpty()) {
                return lista;
            }
        } catch (Exception e) {
            addMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return new ArrayList<Competicao>();
    }

    /**
     * Lista Competicoes disponíveis para exclusao criadas pelo usuario logado
     */
    public List<Competicao> getListaCompeticoes() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession sessao = (HttpSession) facesContext.getExternalContext().getSession(true);
        String idUsuarioLogado = "" + (String) sessao.getAttribute("idUserLogged");
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            tr = session.beginTransaction();
            List<Competicao> lista = session.createQuery(
                    "select c  "
                    + "from Competicao c "
                    + "where c.criador ='" + idUsuarioLogado + "' ", Competicao.class)
                    .getResultList();
            return lista;
        } catch (Exception e) {
            addMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
           
        }
         return new ArrayList<>();
    }
        /**
         * Remove Competicao. Usado na tela areaalodaa para remocao
         */
    public void swipeCompeticao(SwipeEvent event) {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            tr = session.beginTransaction();
            Competicao competicao = (Competicao) event.getData();
            String str = "Removendo: " + competicao.getNome() + ": " + competicao.getData() + "";
            session.delete(competicao);
            tr.commit();
            addMessage(str);
        } catch (Exception e) {
            tr.rollback();
            addMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }

    }

    /**
     * Grava nova competicção em arealogada.xhtml competicao
     */
    public void novaCompeticao() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession sessao = (HttpSession) facesContext.getExternalContext().getSession(true);
        String idUsuarioLogado = "" + (String) sessao.getAttribute("idUserLogged");
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            tr = session.beginTransaction();
            Competicao competicao = new Competicao();
            competicao.setCriador(idUsuarioLogado);
            competicao.setData(date1);
            competicao.setLocal(strLocal);
            competicao.setNome(strNome);
            competicao.setSite(strSite);
            session.save(competicao);
            session.flush();
            tr.commit();
            addMessage("Evento adicionado!");
        } catch (Exception e) {
            if (tr != null && tr.isActive()) {
                try {
                    // Second try catch as the rollback could fail as well
                    tr.rollback();
                } catch (HibernateException e1) {
                    System.out.println("Erro no rollback do transaction");
                }
                // throw again the first exception
                throw e;
            }
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public Competicao getCompeticaoSelecionada() {
        return competicaoSelecionada;
    }

    public void setCompeticaoSelecionada(Competicao competicaoSelecionada) {
        this.competicaoSelecionada = competicaoSelecionada;
    }

    public Date getDate1() {
        return date1;
    }

    public void setDate1(Date date1) {
        this.date1 = date1;
    }

    public String getStrLocal() {
        return strLocal;
    }

    public void setStrLocal(String strLocal) {
        this.strLocal = strLocal;
    }

    public String getStrNome() {
        return strNome;
    }

    public void setStrNome(String strNome) {
        this.strNome = strNome;
    }

    public String getStrSite() {
        return strSite;
    }

    public void setStrSite(String strSite) {
        this.strSite = strSite;
    }

    public String getStrTipoCheckin() {
        return strTipoCheckin;
    }

    public void setStrTipoCheckin(String strTipoCheckin) {
        this.strTipoCheckin = strTipoCheckin;
    }

}
