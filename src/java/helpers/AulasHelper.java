/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

import POJO.Academia;
import POJO.Aprovador;
import POJO.Aula;
import POJO.Categoriaaula;
import POJO.Checkin;
import POJO.CheckinId;
import POJO.Mensagem;
import POJO.Pagamento;
import POJO.Usuario;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import oAuth.UsuarioService;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.mobile.event.SwipeEvent;
import system.HibernateUtil;

/**
 *
 * @author carlos
 */
@Named
@ViewScoped
public class AulasHelper implements Serializable {

    private Aula aulaSelecionada = new Aula();
    private Date date1;
    Checkin checkinstatus = new Checkin();
    private boolean verificaAdmin = false;
    private boolean primeiroLogin = false;
    String dataExibicao = "";
    String stracademia, straula, strhorario, strhorario2, strvagas = "";
    transient HtmlOutputText horarioAula = new HtmlOutputText();
    transient HtmlOutputText idAula = new HtmlOutputText();
    private static String nomeExibicao = "";
    private String imagemExibicao = "";
    private boolean mensagem = true;
    private boolean replica = true;

    public AulasHelper() {
        aulaSelecionada = new Aula();
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession sessao = (HttpSession) facesContext.getExternalContext().getSession(true);
        if (sessao.getAttribute("idUserLogged") == null) {
            loga();
        }
    }

    /**
     * Faz Checkin ou remove checkin, conforme localiza ou não checkin para o
     * usuário logado e aula. Usado em arealogada.xhtml
     */
    public void fazCheckin() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Boolean validaPag = false;
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession sessao = (HttpSession) facesContext.getExternalContext().getSession(true);
        String idUsuarioLogado = "" + (String) sessao.getAttribute("idUserLogged");
        try {
            tr = session.beginTransaction();
            java.util.Date utilDate = new java.util.Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
            if (getAulaSelecionada() != null) {
                int diaAtual = utilDate.getDate();
                int diaAula = aulaSelecionada.getData().getDate();
                int horaAtual = utilDate.getHours() + 1;//servers rodam atrasados -1 nesta instancia
                int horaAula = Integer.parseInt(horarioAula.getValue().toString().substring(0, 2));
                if ((horaAtual > horaAula + 2 && diaAtual == diaAula) || (diaAtual > diaAula)) {
                    addMessage("Horário excedido para checkin");

                } else {
                    /*
                      verifica se há data de vencimento superior a data do
                      checkin para recebimento do checkin com status cria =
                      true
                     */
                    List<Pagamento> listaPagamento = session.createQuery(
                            "select p  "
                            + "from Pagamento p "
                            + "where p.id.datavencimento >='" + sqlDate + "' and p.usuario.email = '" + idUsuarioLogado + "' and p.categoriaaula.aula = 'Jiu-jitsu'", Pagamento.class
                    )
                            .getResultList();
                    if (!listaPagamento.isEmpty() || primeiroLogin) {
                        validaPag = true;
                    } else {
                        validaPag = false;
                    }
                    List<Checkin> listaCheckin = session.createQuery(
                            "select c  "
                            + "from Checkin c where c.aula.idaula ='" + getAulaSelecionada().getIdaula() + "' and c.usuario.email = '" + idUsuarioLogado + "'", Checkin.class
                    )
                            .getResultList();
                    Checkin checkin = new Checkin();

                    if (listaCheckin.isEmpty()) {
                        if (aulaSelecionada.getVagas() > 0) {
                            List<Usuario> listaUsuario = session.createQuery(
                                    "select u  "
                                    + "from Usuario u where u.email = '" + idUsuarioLogado + "'", Usuario.class
                            )
                                    .getResultList();
                            Usuario usuario = new Usuario();
                            usuario = (Usuario) listaUsuario.get(0);
                            checkin.setAula(getAulaSelecionada());
                            checkin.setUsuario(usuario);
                            CheckinId id = new CheckinId();
                            id.setAulaIdaula(getAulaSelecionada().getIdaula());
                            id.setUsuarioEmail(usuario.getEmail());
                            checkin.setId(id);

                            checkin.setCria(validaPag);
                            checkin.setCreateTime(sqlDate);
                            checkin.setConfirma(Boolean.FALSE);
                            aulaSelecionada.setVagas(aulaSelecionada.getVagas() - 1);
                            session.saveOrUpdate(aulaSelecionada);
                            session.merge(checkin);
                            tr.commit();
                            if (!validaPag) {
                                addMessage("Não existe pagamento registrado, seu checkin não será registrado.");
                                PagamentosHelper p = new PagamentosHelper();
                                p.setStrAluno(idUsuarioLogado);
                                p.enviaAviso();
                            } else {
                                addMessage("Checkin recebido");
                            }
                            aulaSelecionada = new Aula();
                        } else {
                            addMessage("Vagas esgotadas");
                        }

                    } else {
                        checkin = (Checkin) listaCheckin.get(0);
                        session.delete(checkin);
                        aulaSelecionada.setVagas(aulaSelecionada.getVagas() + 1);
                        session.saveOrUpdate(aulaSelecionada);
                        session.flush();
                        tr.commit();
                        addMessage("Checkin retirado");
                    }
                }
            }
        } catch (Exception e) {
            if (tr != null && tr.isActive()) {
                try {
// Second try catch as the rollback could fail as well
                    tr.rollback();
                } catch (HibernateException e1) {
                    System.out.println("Erro no rollback do transaction");
                }
// throw again the first exception
                throw e;
            }
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * Listas as próximas aulas a partir de hoje. Usado em arealogada.xhtml
     */
    public List<Aula> getListaProximasAulas() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            tr = session.beginTransaction();
            java.util.Date utilDate = new java.util.Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
            List<Aula> lista = session.createQuery(
                    "select a  "
                    + "from Aula a "
                    + "join fetch a.categoriaaula c "
                    + "join fetch a.academia ac "
                    + "where a.data >='" + sqlDate + "' order by  a.data, a.horario", Aula.class
            )
                    .getResultList();
            if (!lista.isEmpty()) {
                return lista;
            }
        } catch (Exception e) {
            addMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return new ArrayList<Aula>();
    }

    public Aula getAulaSelecionada() {
        return aulaSelecionada;
    }

    public void setAulaSelecionada(Aula aulaSelecionada) {
        this.aulaSelecionada = aulaSelecionada;
    }

    public void onDateSelect(SelectEvent event) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        aulaSelecionada.setData(date1);
        dataExibicao = format.format(event.getObject());
    }

    public Date getDate1() {
        return date1;
    }

    public void setDate1(Date date1) {
        this.date1 = date1;
    }

    /**
     * Remove Aula. Usado na tela Admin em dtlistaula
     */
    public void swipeAula(SwipeEvent event) {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            tr = session.beginTransaction();
            Aula aula = (Aula) event.getData();
            String str = "Removendo: " + aula.getCategoriaaula().getAula() + ": " + aula.getData() + " - " + aula.getHorario();
            session.delete(aula);
            tr.commit();
            addMessage(str);
        } catch (Exception e) {
            tr.rollback();
            addMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }

    }

    /**
     * Lista as categorias para a criação de aula em admim.xhtml addAula e para
     * os relatórios de frequencia em pm:freqAluno e pm:freqGeral
     */
    public Map<String, String> getCategorias() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Categoriaaula categoriaaula = new Categoriaaula();
        try {
            tr = session.beginTransaction();
            Map<String, String> cat = new HashMap<String, String>();
            List<Categoriaaula> lista = session.createQuery(
                    "select c  "
                    + "from Categoriaaula c", Categoriaaula.class
            )
                    .getResultList();
            for (int cont = 0; cont < lista.size(); cont++) {
                categoriaaula = (Categoriaaula) lista.get(cont);
                cat.put(categoriaaula.getAula(), String.valueOf(categoriaaula.getIdcategoriaaula()));
            }
            return cat;
        } catch (Exception e) {
            addMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return new HashMap<String, String>();
    }

    /**
     * Lista as academias para a criação de aula em admim.xhtml addAula
     */
    public Map<String, String> getAcademias() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Academia academia = new Academia();
        try {
            tr = session.beginTransaction();
            Map<String, String> cat = new HashMap<String, String>();
            List<Academia> lista = session.createQuery(
                    "select a  "
                    + "from Academia a", Academia.class
            )
                    .getResultList();
            for (int cont = 0; cont < lista.size(); cont++) {
                academia = (Academia) lista.get(cont);
                cat.put(academia.getAcademia(), String.valueOf(academia.getIdacademia()));
            }
            return cat;
        } catch (Exception e) {
            addMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return new HashMap<String, String>();
    }

    public String getDataExibicao() {
        return dataExibicao;
    }

    public void setDataExibicao(String dataExibicao) {
        this.dataExibicao = dataExibicao;
    }

    /**
     * Grava nova aula em admim.xhtml addAula
     */
    public void novaAula() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            tr = session.beginTransaction();
            java.util.Date utilDate = new Date();
            if (date1 != null) {
                utilDate = date1;
            }
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
            List<Aula> lista = session.createQuery(
                    "select a  "
                    + "from Aula a where a.data = '" + sqlDate + "' and a.horario ='" + strhorario + strhorario2 + "' and a.categoriaaula.idcategoriaaula ='" + Integer.parseInt(straula) + "' and a.academia.idacademia ='" + Integer.parseInt(stracademia) + "'", Aula.class
            )
                    .getResultList();

            if (lista.isEmpty()) {
                List<Academia> listaAcademia = session.createQuery(
                        "select a  "
                        + "from Academia a where a.idacademia = '" + stracademia + "'", Academia.class
                )
                        .getResultList();
                List<Categoriaaula> listaCategoria = session.createQuery(
                        "select c  "
                        + "from Categoriaaula c where c.idcategoriaaula = '" + straula + "'", Categoriaaula.class
                )
                        .getResultList();
                aulaSelecionada.setAcademia((Academia) listaAcademia.get(0));
                aulaSelecionada.setCategoriaaula((Categoriaaula) listaCategoria.get(0));
                aulaSelecionada.setData(utilDate);
                aulaSelecionada.setHorario(strhorario + strhorario2);
                aulaSelecionada.setVagas(Integer.parseInt(strvagas));

                int dia = utilDate.getDay();
                switch (dia) {
                    case 0:
                        aulaSelecionada.setDia("DOM");
                        break;
                    case 1:
                        aulaSelecionada.setDia("SEG");
                        break;
                    case 2:
                        aulaSelecionada.setDia("TER");
                        break;
                    case 3:
                        aulaSelecionada.setDia("QUA");
                        break;
                    case 4:
                        aulaSelecionada.setDia("QUI");
                        break;
                    case 5:
                        aulaSelecionada.setDia("SEX");
                        break;
                    case 6:
                        aulaSelecionada.setDia("SÁB");
                        break;
                }
                session.merge(aulaSelecionada);
                session.flush();
                tr.commit();
                novoAprovador();

                if (replica) {
                    Calendar c = Calendar.getInstance();
                    c.setTime(date1);

                    while (c.getTime().getMonth() <= date1.getMonth()) {
                        c.add(Calendar.DATE, +1);
                        if (c.getTime().getDay() == date1.getDay()) {
                            replicaAula(c);
                        }

                    }
                }

            } else {
                addMessage("Já existe uma aula neste horário");
            }
        } catch (Exception e) {
            if (tr != null && tr.isActive()) {
                try {
                    // Second try catch as the rollback could fail as well
                    tr.rollback();
                } catch (HibernateException e1) {
                    System.out.println("Erro no rollback do transaction");
                }
                // throw again the first exception
                throw e;
            }
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            stracademia = straula = strhorario = strvagas = strhorario2 = "";
            addMessage("Sucesso!");
        }
    }

    /**
     * Complemento do método novaAula() para cadastrar mais aulas até o fim do
     * mês para a aula criada em admim.xhtml addAula
     */
    public void replicaAula(Calendar data) {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();

        try {
            tr = session.beginTransaction();

            java.sql.Date sqlDate = new java.sql.Date(data.getTimeInMillis());
            List<Aula> lista = session.createQuery(
                    "select a  "
                    + "from Aula a where a.data = '" + sqlDate + "' and a.horario ='" + strhorario + strhorario2 + "' and a.categoriaaula.idcategoriaaula ='" + Integer.parseInt(straula) + "' and a.academia.idacademia ='" + Integer.parseInt(stracademia) + "'", Aula.class
            )
                    .getResultList();

            if (lista.isEmpty()) {
                List<Academia> listaAcademia = session.createQuery(
                        "select a  "
                        + "from Academia a where a.idacademia = '" + stracademia + "'", Academia.class
                )
                        .getResultList();
                List<Categoriaaula> listaCategoria = session.createQuery(
                        "select c  "
                        + "from Categoriaaula c where c.idcategoriaaula = '" + straula + "'", Categoriaaula.class
                )
                        .getResultList();
                aulaSelecionada.setAcademia((Academia) listaAcademia.get(0));
                aulaSelecionada.setCategoriaaula((Categoriaaula) listaCategoria.get(0));
                aulaSelecionada.setData(sqlDate);
                aulaSelecionada.setHorario(strhorario + strhorario2);
                aulaSelecionada.setVagas(Integer.parseInt(strvagas));

                int dia = sqlDate.getDay();
                switch (dia) {
                    case 0:
                        aulaSelecionada.setDia("DOM");
                        break;
                    case 1:
                        aulaSelecionada.setDia("SEG");
                        break;
                    case 2:
                        aulaSelecionada.setDia("TER");
                        break;
                    case 3:
                        aulaSelecionada.setDia("QUA");
                        break;
                    case 4:
                        aulaSelecionada.setDia("QUI");
                        break;
                    case 5:
                        aulaSelecionada.setDia("SEX");
                        break;
                    case 6:
                        aulaSelecionada.setDia("SÁB");
                        break;
                }
                session.save(aulaSelecionada);
                session.flush();
                tr.commit();
                novoAprovador();

            }
        } catch (Exception e) {
            if (tr != null && tr.isActive()) {
                try {
                    // Second try catch as the rollback could fail as well
                    tr.rollback();
                } catch (HibernateException e1) {
                    System.out.println("Erro no rollback do transaction");
                }
                // throw again the first exception
                throw e;
            }
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    /**
     * Complemento do método novaAula() para cadastrar também um aprovador para
     * a aula criada em admim.xhtml addAula
     */
    public void novoAprovador() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession sessao = (HttpSession) facesContext.getExternalContext().getSession(true);
        String idUsuarioLogado = "" + (String) sessao.getAttribute("idUserLogged");
        try {
            java.sql.Date sqlDate = new java.sql.Date(aulaSelecionada.getData().getTime());
            tr = session.beginTransaction();
            List<Aula> listaAula = session.createQuery(
                    "select a  "
                    + "from Aula a where a.data = '" + sqlDate + "' and a.horario ='" + strhorario + strhorario2 + "' and a.categoriaaula.idcategoriaaula ='" + Integer.parseInt(straula) + "' and a.academia.idacademia ='" + Integer.parseInt(stracademia) + "'", Aula.class
            )
                    .getResultList();
            List<Usuario> listaUsuario = session.createQuery(
                    "select u  "
                    + "from Usuario u where u.email ='" + idUsuarioLogado + "'", Usuario.class
            )
                    .getResultList();
            if (!listaAula.isEmpty() && !listaUsuario.isEmpty()) {
                Aula aula = new Aula();
                aula = (Aula) listaAula.get(0);
                Usuario usuario = new Usuario();
                usuario = (Usuario) listaUsuario.get(0);
                Aprovador aprovador = new Aprovador();
                aprovador.setAula(aula);
                aprovador.setAulaIdaula(aula.getIdaula());
                aprovador.setUsuario(usuario);
                session.save(aprovador);
                tr.commit();
                aulaSelecionada = new Aula();
                aprovador = new Aprovador();
            } else {
                addMessage("Aula ou usuário aprovador não localizado");
                tr.rollback();
                session.clear();
            }
        } catch (Exception e) {
            if (tr != null && tr.isActive()) {
                try {
                    // Second try catch as the rollback could fail as well
                    tr.rollback();
                } catch (HibernateException e1) {
                    System.out.println("Erro no rollback do transaction");
                }
                // throw again the first exception
                throw e;
            }
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }

    /**
     * Sinaliza o status de cada checkin das Aulas apresentadas do Datalist da
     * arealogada. Este status renderiza os botões de checkin e o ícone de
     * aprovado(estrela) Usado em arealogada.xhtml
     */
    public Checkin getCheckinstatus() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession sessao = (HttpSession) facesContext.getExternalContext().getSession(true);
        String idUsuarioLogado = "" + (String) sessao.getAttribute("idUserLogged");
        try {
            tr = session.beginTransaction();
            List<Checkin> lista = session.createQuery(
                    "select c  "
                    + "from Checkin c where c.usuario.email = '" + idUsuarioLogado + "' and c.aula.idaula = '" + idAula.getValue() + "'", Checkin.class
            )
                    .getResultList();
            if (!lista.isEmpty()) {
                Checkin checkin = new Checkin();
                checkin = (Checkin) lista.get(0);
                return checkin;
            }
        } catch (Exception e) {
            tr.rollback();
            addMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
        return checkinstatus;
    }

    public void loga() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        POJO.Usuario usuario = new POJO.Usuario();
        try {
            tr = session.beginTransaction();
            List<POJO.Usuario> listaUsuario = session.createQuery(
                    "select u  "
                    + "from Usuario u where u.email = '" + UsuarioService.stremail + "'", POJO.Usuario.class)
                    .getResultList();
            if (listaUsuario.isEmpty()) {

                usuario.setNome(UsuarioService.struser);
                usuario.setEmail(UsuarioService.stremail);
                session.save(usuario);
                tr.commit();
                FacesContext facesContext = FacesContext.getCurrentInstance();
                HttpSession sessao = (HttpSession) facesContext.getExternalContext().getSession(true);
                sessao.setAttribute("idUserLogged", UsuarioService.stremail);
                nomeExibicao = UsuarioService.struser;
                imagemExibicao = UsuarioService.strpicture;
                primeiroLogin = true;
            } else {
                FacesContext facesContext = FacesContext.getCurrentInstance();
                HttpSession sessao = (HttpSession) facesContext.getExternalContext().getSession(true);
                sessao.setAttribute("idUserLogged", UsuarioService.stremail);
                nomeExibicao = UsuarioService.struser;
                imagemExibicao = UsuarioService.strpicture;
            }
        } catch (Exception e) {
            if (tr != null && tr.isActive()) {
                try {
// Second try catch as the rollback could fail as well
                    tr.rollback();
                } catch (HibernateException e1) {
                    System.out.println("Erro no rollback do transaction");
                }
// throw again the first exception
                throw e;
            }
        } finally {
            if (session != null) {
                try {
                    session.close();
                    testaMensagens();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Lista Mensagens disponíveis
     */
    public Mensagem getMensagens() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            tr = session.beginTransaction();
            Calendar a = Calendar.getInstance();
            a.setTime(new Date());//data maior
            a.add(Calendar.DAY_OF_MONTH, -6);
            java.sql.Date sqlDate = new java.sql.Date(a.getTimeInMillis());
            List<Mensagem> lista = session.createQuery(
                    "select m  "
                    + "from Mensagem m "
                    + "where m.data >= '" + sqlDate + "' order by m.idmensagem asc", Mensagem.class)
                    .getResultList();
            if (!lista.isEmpty()) {
                setMensagem(true);
                return lista.get(0);
            }

        } catch (Exception e) {
            tr.rollback();
            addMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
        return new Mensagem();
    }

    /**
     * Lista Mensagens disponíveis
     */
    public void testaMensagens() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            tr = session.beginTransaction();
            Calendar a = Calendar.getInstance();
            a.setTime(new Date());//data maior
            a.add(Calendar.DAY_OF_MONTH, -6);
            java.sql.Date sqlDate = new java.sql.Date(a.getTimeInMillis());
            List<Mensagem> lista = session.createQuery(
                    "select m  "
                    + "from Mensagem m "
                    + "where m.data >= '" + sqlDate + "' order by m.idmensagem desc", Mensagem.class)
                    .getResultList();
            if (!lista.isEmpty()) {
                RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
            }

        } catch (Exception e) {
            tr.rollback();
            addMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }

    }

    public boolean isVerificaAdmin() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession sessao = (HttpSession) facesContext.getExternalContext().getSession(true);
        String idUsuarioLogado = "" + (String) sessao.getAttribute("idUserLogged");
        if (idUsuarioLogado.matches("carlosalan2@hotmail.com") || idUsuarioLogado.matches("danielbjj@hotmail.com")) {
            return true;
        }
        return false;
    }

    public String getStracademia() {
        return stracademia;
    }

    public void setStracademia(String stracademia) {
        this.stracademia = stracademia;
    }

    public String getStraula() {
        return straula;
    }

    public void setStraula(String straula) {
        this.straula = straula;
    }

    public String getStrhorario() {
        return strhorario;
    }

    public void setStrhorario(String strhorario) {
        this.strhorario = strhorario;
    }

    public String getStrvagas() {
        return strvagas;
    }

    public void setStrvagas(String strvagas) {
        this.strvagas = strvagas;
    }

    public HtmlOutputText getHorarioAula() {
        return horarioAula;
    }

    public void setHorarioAula(HtmlOutputText horarioAula) {
        this.horarioAula = horarioAula;
    }

    public HtmlOutputText getIdAula() {
        return idAula;
    }

    public void setIdAula(HtmlOutputText idAula) {
        this.idAula = idAula;
    }

    public void setCheckinstatus(Checkin checkinstatus) {
        this.checkinstatus = checkinstatus;
    }

    public static String getNomeExibicao() {
        return nomeExibicao;
    }

    public static void setNomeExibicao(String nomeExibicao) {
        AulasHelper.nomeExibicao = nomeExibicao;
    }

    public String getImagemExibicao() {
        return imagemExibicao;
    }

    public void setImagemExibicao(String imagemExibicao) {
        this.imagemExibicao = imagemExibicao;
    }

    public String getStrhorario2() {
        return strhorario2;
    }

    public void setStrhorario2(String strhorario2) {
        this.strhorario2 = strhorario2;
    }

    public boolean isPrimeiroLogin() {
        return primeiroLogin;
    }

    public void setPrimeiroLogin(boolean primeiroLogin) {
        this.primeiroLogin = primeiroLogin;
    }

    public boolean isMensagem() {
        return mensagem;
    }

    public void setMensagem(boolean mensagem) {
        this.mensagem = mensagem;
    }

    public boolean isReplica() {
        return replica;
    }

    public void setReplica(boolean replica) {
        this.replica = replica;
    }

}
