/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

import POJO.Aviso;
import POJO.Categoriaaula;
import POJO.Mensagem;
import POJO.Pagamento;
import POJO.PagamentoId;
import POJO.Usuario;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.ejb.Stateless;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.persistence.PostLoad;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.primefaces.mobile.event.SwipeEvent;
import system.HibernateUtil;

/**
 *
 * @author carlos
 */
@Named
@ViewScoped
public class PagamentosHelper implements Serializable {

    private Date date1;
    private Date date2;
    private String cat, strAluno, strAula, strValor, strForma = "";
    private String strPeriodoPag = "proximos";
    private String strAlunoAviso = "todos";
    private String limpeza = "";
    private String strMsg = "";

    /**
     * Cria pagamentos. Usado em gerpagamento em admin.xhtml
     */
    public void criaPagamento() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Pagamento pagamentoatual = new Pagamento();
        PagamentoId pagamentoatualId = new PagamentoId();
        try {
            tr = session.beginTransaction();
            java.util.Date utilDate = new Date();

            if (date1 != null && date2 == null) {
                //cadastra um pagamento
                List<Usuario> listaAluno = session.createQuery(
                        "select u  "
                        + "from Usuario u where u.email = '" + strAluno + "'", Usuario.class)
                        .getResultList();
                List<Categoriaaula> listaCategoria = session.createQuery(
                        "select c  "
                        + "from Categoriaaula c where c.idcategoriaaula = '" + strAula + "'", Categoriaaula.class)
                        .getResultList();
                pagamentoatual.setCategoriaaula(listaCategoria.get(0));
                pagamentoatual.setForma(strForma);
                pagamentoatual.setPago(Boolean.TRUE);
                pagamentoatual.setUsuario(listaAluno.get(0));
                pagamentoatualId.setDatavencimento(date1);
                pagamentoatualId.setUsuarioEmail(listaAluno.get(0).getEmail());
                pagamentoatualId.setValor(Double.valueOf(strValor).longValue());
                pagamentoatual.setId(pagamentoatualId);
                session.saveOrUpdate(pagamentoatual);
                tr.commit();
                date1 = null;
                date2 = null;
            } else {
                //cadastra diversos pagamentos
                List<Usuario> listaAluno = session.createQuery(
                        "select u  "
                        + "from Usuario u where u.email = '" + strAluno + "'", Usuario.class)
                        .getResultList();
                List<Categoriaaula> listaCategoria = session.createQuery(
                        "select c  "
                        + "from Categoriaaula c where c.idcategoriaaula = '" + strAula + "'", Categoriaaula.class)
                        .getResultList();
                if (date1.before(date2)) {
                    int meses = (int) getDateDiff(date1, date2, TimeUnit.DAYS) / 30;
                    Date mesAtual = new Date(date1.getTime());

                    pagamentoatual.setCategoriaaula(listaCategoria.get(0));
                    pagamentoatual.setForma(strForma);
                    pagamentoatual.setPago(Boolean.TRUE);
                    pagamentoatual.setUsuario(listaAluno.get(0));
                    pagamentoatualId.setDatavencimento(mesAtual);
                    pagamentoatualId.setUsuarioEmail(listaAluno.get(0).getEmail());
                    pagamentoatualId.setValor(Double.valueOf(strValor).longValue());
                    pagamentoatual.setId(pagamentoatualId);
                    session.saveOrUpdate(pagamentoatual);
                    tr.commit();

                    for (int cont = 2; cont <= meses + 1; cont++) {
                        pagamentoatual = new Pagamento();
                        pagamentoatualId = new PagamentoId();
                        int mes = mesAtual.getMonth() + 1;
                        switch (mes) {
                            case 1:
                                mesAtual = mes31(mesAtual);
                                criaPagamentoIterativo(listaAluno, listaCategoria, mesAtual);
                                break;
                            case 2:
                                mesAtual = mes28(mesAtual);
                                criaPagamentoIterativo(listaAluno, listaCategoria, mesAtual);
                                break;
                            case 3:
                                mesAtual = mes31(mesAtual);
                                criaPagamentoIterativo(listaAluno, listaCategoria, mesAtual);
                                break;
                            case 4:
                                mesAtual = mes30(mesAtual);
                                criaPagamentoIterativo(listaAluno, listaCategoria, mesAtual);
                                break;
                            case 5:
                                mesAtual = mes31(mesAtual);
                                criaPagamentoIterativo(listaAluno, listaCategoria, mesAtual);
                                break;
                            case 6:
                                mesAtual = mes30(mesAtual);
                                criaPagamentoIterativo(listaAluno, listaCategoria, mesAtual);
                                break;
                            case 7:
                                mesAtual = mes31(mesAtual);
                                criaPagamentoIterativo(listaAluno, listaCategoria, mesAtual);
                                break;
                            case 8:
                                mesAtual = mes31(mesAtual);
                                criaPagamentoIterativo(listaAluno, listaCategoria, mesAtual);
                                break;
                            case 9:
                                mesAtual = mes30(mesAtual);
                                criaPagamentoIterativo(listaAluno, listaCategoria, mesAtual);
                                break;
                            case 10:
                                mesAtual = mes31(mesAtual);
                                criaPagamentoIterativo(listaAluno, listaCategoria, mesAtual);
                                break;
                            case 11:
                                mesAtual = mes30(mesAtual);
                                criaPagamentoIterativo(listaAluno, listaCategoria, mesAtual);
                                break;
                            case 12:
                                mesAtual = mes31(mesAtual);
                                criaPagamentoIterativo(listaAluno, listaCategoria, mesAtual);
                                break;
                        }
                    }

                } else {
                    addMessage("O último vencimento deve ser posterior ao primeiro");
                }
                tr.commit();
                addMessage("Pagamentos atualizados.");
            }

        } catch (Exception e) {
            if (tr != null && tr.isActive()) {
                try {
                    // Second try catch as the rollback could fail as well
                    tr.rollback();
                } catch (HibernateException e1) {
                    System.out.println("Erro no rollback do transaction");
                }
                // throw again the first exception
                throw e;
            }
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public void criaPagamentoIterativo(List<Usuario> listaAluno, List<Categoriaaula> listaCategoria, Date mesAtual) {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Pagamento pagamentoatual = new Pagamento();
        PagamentoId pagamentoatualId = new PagamentoId();
        try {
            tr = session.beginTransaction();
            java.util.Date utilDate = new Date();
            pagamentoatual.setCategoriaaula(listaCategoria.get(0));
            pagamentoatual.setForma(strForma);
            pagamentoatual.setPago(Boolean.TRUE);
            pagamentoatual.setUsuario(listaAluno.get(0));
            pagamentoatualId.setDatavencimento(mesAtual);
            pagamentoatualId.setUsuarioEmail(listaAluno.get(0).getEmail());
            pagamentoatualId.setValor(Double.valueOf(strValor).longValue());
            pagamentoatual.setId(pagamentoatualId);
            session.saveOrUpdate(pagamentoatual);
            tr.commit();
        } catch (Exception e) {
            if (tr != null && tr.isActive()) {
                try {
                    // Second try catch as the rollback could fail as well
                    tr.rollback();
                } catch (HibernateException e1) {
                    System.out.println("Erro no rollback do transaction");
                }
                // throw again the first exception
                throw e;
            }
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public Date mes28(Date mesAtualizado) {
        for (int cont2 = 0; cont2 < 28; cont2++) {
            mesAtualizado.setTime(mesAtualizado.getTime() + (1000 * 60 * 60 * 24));
        }
        return mesAtualizado;
    }

    public Date mes30(Date mesAtualizado) {
        for (int cont2 = 0; cont2 < 30; cont2++) {
            mesAtualizado.setTime(mesAtualizado.getTime() + (1000 * 60 * 60 * 24));
        }
        return mesAtualizado;
    }

    public Date mes31(Date mesAtualizado) {
        for (int cont2 = 0; cont2 < 31; cont2++) {
            mesAtualizado.setTime(mesAtualizado.getTime() + (1000 * 60 * 60 * 24));
        }
        return mesAtualizado;
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * Listas as próximas pagamentos ou pagamentos passados baseado
     * p:selectOneButton na tela. Usado em admin.xhtml gerPagamento
     */
    public List<Pagamento> getListaPagamentos() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            tr = session.beginTransaction();
            java.util.Date utilDate = new java.util.Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
            if (strPeriodoPag.matches("anteriores")) {
                List<Pagamento> lista = session.createQuery(
                        "select p  "
                        + "from Pagamento p "
                        + "join fetch p.categoriaaula c "
                        + "where p.id.datavencimento <='" + sqlDate + "' and p.usuario.email = '" + strAluno + "' order by p.id.datavencimento asc", Pagamento.class)
                        .getResultList();
                if (!lista.isEmpty()) {
                    return lista;
                }

            } else {
                List<Pagamento> lista = session.createQuery(
                        "select p  "
                        + "from Pagamento p "
                        + "join fetch p.categoriaaula c "
                        + "where p.id.datavencimento >'" + sqlDate + "' and p.usuario.email = '" + strAluno + "' order by p.id.datavencimento asc", Pagamento.class)
                        .getResultList();
                if (!lista.isEmpty()) {
                    return lista;
                }
            }
        } catch (Exception e) {
            addMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return new ArrayList<Pagamento>();
    }

    /**
     * Envia Aviso para usuários que na presente data não possuirem pagamento
     * cadastrado (Data Atual > último vencimento)
     */
    public void enviaAviso() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Calendar a = Calendar.getInstance();
        a.setTime(new Date());//data maior
        a.add(Calendar.DAY_OF_MONTH, -1);
        java.sql.Date sqlDate = new java.sql.Date(a.getTimeInMillis());
        List<Usuario> listaAluno = new ArrayList<>();
        try {
            tr = session.beginTransaction();

            //levanta alunos sem pagamentos
            if (strAluno.matches("todos")) {
                listaAluno = session.createQuery(
                        "select u "
                        + "from Usuario u "
                        + "join fetch u.checkins c "
                        + "where c.createTime >= '" + sqlDate + "' and c.cria = 0", Usuario.class)
                        .getResultList();
            } else {
                listaAluno = session.createQuery(
                        "select u "
                        + "from Usuario u "
                        + "join fetch u.checkins c "
                        + "where c.createTime >= '" + sqlDate + "' and c.cria = 0 and u.email='" + strAluno + "' ", Usuario.class)
                        .getResultList();
            }

        } catch (Exception e) {
            if (tr != null && tr.isActive()) {
                try {
                    // Second try catch as the rollback could fail as well
                    tr.rollback();
                } catch (HibernateException e1) {
                    System.out.println("Erro no rollback do transaction");
                }
                // throw again the first exception
                throw e;
            }
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (!listaAluno.isEmpty()) {
                for (int cont = 0; cont < listaAluno.size(); cont++) {
                    criaAvisoIterativo(listaAluno.get(cont));
                }
            }
            addMessage("Avisos enviados.");
        }

    }

    public void criaAvisoIterativo(Usuario aluno) {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            tr = session.beginTransaction();
            Aviso aviso = new Aviso();
            aviso.setUsuario(aluno);
            aviso.setMensagem("Checkins não computados. Verifique pagamentos.");
            aviso.setData((new Date()));
            aviso.setAutor("Sistema");
            aviso.setLida(Boolean.FALSE);
            session.save(aviso);
            tr.commit();
        } catch (Exception e) {
            if (tr != null && tr.isActive()) {
                try {
                    // Second try catch as the rollback could fail as well
                    tr.rollback();
                } catch (HibernateException e1) {
                    System.out.println("Erro no rollback do transaction");
                }
                // throw again the first exception
                throw e;
            }
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public void enviaMensagem() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().openSession();
        try {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            HttpSession sessao = (HttpSession) facesContext.getExternalContext().getSession(true);
            String idUsuarioLogado = "" + (String) sessao.getAttribute("idUserLogged");
            List<Usuario> listaAluno = session.createQuery(
                    "select u  "
                    + "from Usuario u where u.email = '" + idUsuarioLogado + "'", Usuario.class)
                    .getResultList();
            Calendar a = Calendar.getInstance();
            a.setTime(new Date());//data maior
            tr = session.beginTransaction();
            Mensagem msg = new Mensagem();
            msg.setMensagem(strMsg);
            msg.setAutor(AulasHelper.getNomeExibicao());
            msg.setData(a.getTime());
            msg.setUsuario(listaAluno.get(0));
            session.save(msg);
            tr.commit();
            addMessage("Mensagem enviada para todos os usuários.");
        } catch (Exception e) {
            if (tr != null && tr.isActive()) {
                try {
                    // Second try catch as the rollback could fail as well
                    tr.rollback();
                } catch (HibernateException e1) {
                    System.out.println("Erro no rollback do transaction");
                }
                // throw again the first exception
                throw e;
            }
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    /**
     * Remove Pagamento. Usado na tela Admin em gerpagamento
     */
    public void swipePagamento(SwipeEvent event) {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            tr = session.beginTransaction();
            Pagamento pagamento = (Pagamento) event.getData();
            String str = "Removendo: " + pagamento.getId().getDatavencimento() + ": " + pagamento.getId().getValor();
            session.delete(pagamento);
            tr.commit();
            addMessage(str);
        } catch (Exception e) {
            tr.rollback();
            addMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }

    /**
     * Lista Usuário com checkin cria = 0 (tentativa e sem pagamento) Usado em
     * dispAvisosPag em admin.xhtml
     */
    public Map<String, String> getUsuariosAtrasoMap() {
        Map<String, String> map = new HashMap<String, String>();
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Calendar a = Calendar.getInstance();
        a.setTime(new Date());//data maior
        a.add(Calendar.DAY_OF_MONTH, -1);
        java.sql.Date sqlDate = new java.sql.Date(a.getTimeInMillis());
        try {
            List<Usuario> listaAluno = new ArrayList<>();
            tr = session.beginTransaction();
            Usuario usuario = new Usuario();

            listaAluno = session.createQuery(
                    "select u "
                    + "from Usuario u "
                    + "join fetch u.checkins c "
                    + "where c.createTime >= '" + sqlDate + "' and c.cria = 0", Usuario.class)
                    .getResultList();
            for (int cont = 0; cont < listaAluno.size(); cont++) {
                usuario = (Usuario) listaAluno.get(cont);
                map.put(usuario.getNome(), usuario.getEmail());
            }
            return map;
        } catch (Exception e) {
            tr.rollback();
            addMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
        return map;
    }

    /**
     * Sinaliza se há avisos para o usuário logado. Este status renderiza os
     * ícone de aviso.
     */
    public List<Aviso> getAvisostatus() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession sessao = (HttpSession) facesContext.getExternalContext().getSession(true);
        String idUsuarioLogado = "" + (String) sessao.getAttribute("idUserLogged");
        try {
            tr = session.beginTransaction();
            List<Aviso> lista = session.createQuery(
                    "select a  "
                    + "from Aviso a where a.usuario.email = '" + idUsuarioLogado + "' and a.lida = '" + 0 + "' ", Aviso.class)
                    .getResultList();
            if (!lista.isEmpty()) {
                return lista;
            }
        } catch (Exception e) {
            tr.rollback();
            addMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return new ArrayList<Aviso>();
    }

    /**
     * Limpa avisos depois de lidos. Usado na página avisos.xhtml após clicar no
     * ícone de aviso.
     */
    public void limpaAvisos() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession sessao = (HttpSession) facesContext.getExternalContext().getSession(true);
        String idUsuarioLogado = "" + (String) sessao.getAttribute("idUserLogged");
        try {
            tr = session.beginTransaction();
            session.createNativeQuery(
                    "UPDATE tf.aviso a SET a.lida = '1' where a.lida = '0' and a.usuario_email='" + idUsuarioLogado + "'").executeUpdate();
            tr.commit();
        } catch (Exception e) {
            //tr.rollback();
            addMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Lista Usuário com checkin cria = 1 mas sem pagamento (Primeiro login)
     * Usado em gerpagamento em admin.xhtml
     */
    public List<Object[]> getUsuariosPrimeiroLogin() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        Calendar a = Calendar.getInstance();
        a.setTime(new Date());//data maior
        a.add(Calendar.DAY_OF_MONTH, -5);
        java.sql.Date sqlDate = new java.sql.Date(a.getTimeInMillis());
        try {
            tr = session.beginTransaction();
            List<Object[]> lista = session.createNativeQuery(
                    "SELECT u.nome, ct.aula FROM tf.checkin c inner join tf.usuario u on u.email = c.usuario_email inner join tf.aula a on a.idaula = c.aula_idaula inner join tf.categoriaaula ct on ct.idcategoriaaula = a.categoriaaula_idcategoriaaula where c.create_time >= '" + sqlDate + "' and u.email not in (select p.usuario_email from tf.pagamento p where p.datavencimento >= '" + sqlDate + "' ) order by u.nome asc").getResultList();
            return lista;
        } catch (Exception e) {
            tr.rollback();
            addMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
        List<Object[]> lista = new ArrayList<>();
        return lista;
    }

    public String getStrPeriodoPag() {
        return strPeriodoPag;
    }

    public void setStrPeriodoPag(String strPeriodoPag) {
        this.strPeriodoPag = strPeriodoPag;
    }

    public Date getDate1() {
        return date1;
    }

    public void setDate1(Date date1) {
        this.date1 = date1;
    }

    public Date getDate2() {
        return date2;
    }

    public void setDate2(Date date2) {
        this.date2 = date2;
    }

    public String getCat() {
        return cat;
    }

    public void setCat(String cat) {
        this.cat = cat;
    }

    public String getStrAluno() {
        return strAluno;
    }

    public void setStrAluno(String strAluno) {
        this.strAluno = strAluno;
    }

    public String getStrAula() {
        return strAula;
    }

    public void setStrAula(String strAula) {
        this.strAula = strAula;
    }

    public String getStrValor() {
        return strValor;
    }

    public void setStrValor(String strValor) {
        this.strValor = strValor;
    }

    public String getStrForma() {
        return strForma;
    }

    public void setStrForma(String strForma) {
        this.strForma = strForma;
    }

    public String getStrAlunoAviso() {
        return strAlunoAviso;
    }

    public void setStrAlunoAviso(String strAlunoAviso) {
        this.strAlunoAviso = strAlunoAviso;
    }

    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }

    public String getLimpeza() {
        limpaAvisos();
        return limpeza;
    }

    public void setLimpeza(String limpeza) {
        this.limpeza = limpeza;
    }

    public String getStrMsg() {
        return strMsg;
    }

    public void setStrMsg(String strMsg) {
        this.strMsg = strMsg;
    }

}
