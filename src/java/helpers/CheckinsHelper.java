/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

import POJO.Aprovador;
import POJO.Aula;
import POJO.Checkin;
import POJO.Checkincompeticao;
import POJO.Checkinseminario;
import POJO.Usuario;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.primefaces.mobile.event.SwipeEvent;
import system.HibernateUtil;

/**
 *
 * @author carlos
 */
@Named
@ViewScoped
public class CheckinsHelper implements Serializable {

    List<Checkin> listaCheckin = new ArrayList<Checkin>();
    Map<String, String> aulas = new HashMap<String, String>();
    String straula = "";
    Checkin checkinSelecionado = new Checkin();
    String strusuario = "";

    /**
     * Lista Checkins disponíveis para aprovação. Regra: c.confirma = 0 and
     * c.updateTime is null. Onde:Tela aprovação.xhtml.
     */
    public List<Checkin> getListaCheckin() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        if (!straula.isEmpty()) {
            try {
                tr = session.beginTransaction();
                Calendar a = Calendar.getInstance();
                a.setTime(new Date());//data maior
                a.add(Calendar.DAY_OF_MONTH, -6);
                java.sql.Date sqlDate = new java.sql.Date(a.getTimeInMillis());
                List<Checkin> lista = session.createQuery(
                        "select c  "
                        + "from Checkin c "
                        + "join fetch c.usuario u "
                        + "where c.createTime >='" + sqlDate + "' and c.aula.idaula = '" + Integer.parseInt(straula) + "' and c.confirma = 0 and c.updateTime is null order by u.nome asc", Checkin.class)
                        .getResultList();
                return lista;
            } catch (Exception e) {
                addMessage(e.getMessage());
                e.printStackTrace();
            } finally {
                if (session != null) {
                    try {
                        session.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

            }
        }
        return listaCheckin;
    }

    /**
     * Lista Checkins em competições disponíveis para aprovação.
     */
    public List<Checkincompeticao> getListaCheckinCompeticao() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            tr = session.beginTransaction();
            Calendar a = Calendar.getInstance();
            a.setTime(new Date());//data maior
            a.add(Calendar.DAY_OF_MONTH, -6);
            java.sql.Date sqlDate = new java.sql.Date(a.getTimeInMillis());
            List<Checkincompeticao> lista = session.createQuery(
                    "select c  "
                    + "from Checkincompeticao c "
                    + "join fetch c.usuario u "
                    + "where c.createTime >='" + sqlDate + "' and c.updateTime is null order by c.usuario.nome asc", Checkincompeticao.class)
                    .getResultList();
            return lista;
        } catch (Exception e) {
            addMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }

        return new ArrayList<>();
    }

    /**
     * Lista Checkins em seminários disponíveis para aprovação.
     */
    public List<Checkinseminario> getListaCheckinSeminario() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            tr = session.beginTransaction();
            Calendar a = Calendar.getInstance();
            a.setTime(new Date());//data maior
            a.add(Calendar.DAY_OF_MONTH, -6);
            java.sql.Date sqlDate = new java.sql.Date(a.getTimeInMillis());
            List<Checkinseminario> lista = session.createQuery(
                    "select c  "
                    + "from Checkinseminario c "
                    + "join fetch c.usuario u "
                    + "where c.createTime >='" + sqlDate + "' and c.updateTime is null order by c.usuario.nome asc", Checkinseminario.class)
                    .getResultList();
            return lista;
        } catch (Exception e) {
            addMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        }

        return new ArrayList<>();
    }

    /**
     * Lista Aulas da última semana disponíveis para atribuir aprovador. Regra:
     * a.data dos 6 dias anteriores. Onde:Tela admin.xhtml.
     */
    public Map<String, String> getAulasRecentes() {
        Map<String, String> cat = new HashMap<String, String>();
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            tr = session.beginTransaction();
            Aula aula = new Aula();

            Calendar a = Calendar.getInstance();
            a.setTime(new Date());//data maior
            a.add(Calendar.DAY_OF_MONTH, -6);
            java.sql.Date sqlDate = new java.sql.Date(a.getTimeInMillis());
            List<Aula> lista = session.createQuery(
                    "select a  "
                    + "from Aula a "
                    + "where a.data >='" + sqlDate + "' order by a.data asc", Aula.class)
                    .getResultList();
            for (int cont = 0; cont < lista.size(); cont++) {
                aula = (Aula) lista.get(cont);
                cat.put(aula.getData().getDate() + "/" + (aula.getData().getMonth() + 1) + " " + aula.getAcademia().getAcademia() + " " + aula.getCategoriaaula().getAula() + " " + aula.getDia() + " " + aula.getHorario(), String.valueOf(aula.getIdaula()));
            }
            return cat;
        } catch (Exception e) {
            tr.rollback();
            addMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
        return cat;
    }

    /**
     * Lista Aulas da última semana disponíveis para aprovação de checkins.
     * Regra: a.data dos 6 dias anteriores. e existe checkin para ser aprovado.
     * Onde:Tela aprovação.xhtml.
     */
    public Map<String, String> getAulasRecentesAprovar() {
        Map<String, String> cat = new HashMap<String, String>();
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession sessao = (HttpSession) facesContext.getExternalContext().getSession(true);
        String idUsuarioLogado = "" + (String) sessao.getAttribute("idUserLogged");
        try {
            tr = session.beginTransaction();
            Aula aula = new Aula();

            Calendar a = Calendar.getInstance();
            a.setTime(new Date());//data maior
            a.add(Calendar.DAY_OF_MONTH, -6);
            java.sql.Date sqlDate = new java.sql.Date(a.getTimeInMillis());
            List<Aula> lista = session.createQuery(
                    "select a  "
                    + "from Aula a "
                    + "join fetch a.checkins c "
                    + "where a.data >= '" + sqlDate + "' and c.confirma = 0 and c.updateTime is null and c.cria = 1 and a.aprovador.usuario.email = '" + idUsuarioLogado + "' order by a.data asc)", Aula.class)
                    .getResultList();
            for (int cont = 0; cont < lista.size(); cont++) {
                aula = (Aula) lista.get(cont);
                cat.put(aula.getData().getDate() + "/" + (aula.getData().getMonth() + 1) + " " + aula.getAcademia().getAcademia() + " " + aula.getCategoriaaula().getAula() + " " + aula.getDia() + " " + aula.getHorario(), String.valueOf(aula.getIdaula()));
            }
            return cat;
        } catch (Exception e) {
            tr.rollback();
            addMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
        return cat;
    }

    /**
     * Lista Usuário aprovador para uma aula selecionada. Usado em addAprovacao
     * em admin.xhtml
     */
    public ArrayList<String> getUsuarioAprovador() {
        ArrayList<String> arrayList = new ArrayList();
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        if (!straula.isEmpty()) {
            try {
                tr = session.beginTransaction();
                Aprovador aprovador = new Aprovador();

                Map<String, String> map = new HashMap<String, String>();
                List<Aprovador> lista = session.createQuery(
                        "select ap  "
                        + "from Aprovador ap "
                        + "where ap.aula.idaula ='" + straula + "'", Aprovador.class)
                        .getResultList();
                if (!lista.isEmpty()) {
                    for (int cont = 0; cont < lista.size(); cont++) {
                        aprovador = (Aprovador) lista.get(cont);
                        arrayList.add("Aprovador atual: " + aprovador.getUsuario().getNome());
                        arrayList.add(aprovador.getUsuario().getEmail());
                        aprovador = new Aprovador();

                    }
                } else {
                    arrayList.add("");
                    arrayList.add("");
                }
                return arrayList;
            } catch (Exception e) {
                tr.rollback();
                addMessage(e.getMessage());
                e.printStackTrace();
            } finally {
                if (session != null) {
                    try {
                        session.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }
        arrayList.add("Escolher Aprovador");
        arrayList.add("");
        return arrayList;
    }

    /**
     * Lista Usuário possíveis de serem aprovadores para uma aula selecionada.
     * Usado em addAprovacao em admin.xhtml
     */
    public Map<String, String> getUsuariosMap() {
        Map<String, String> map = new HashMap<String, String>();
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            tr = session.beginTransaction();
            Usuario usuario = new Usuario();

            List<Usuario> lista = session.createQuery(
                    "select u  "
                    + "from Usuario u order by u.nome asc", Usuario.class)
                    .getResultList();
            for (int cont = 0; cont < lista.size(); cont++) {
                usuario = (Usuario) lista.get(cont);
                map.put(usuario.getNome(), usuario.getEmail());
            }
            return map;
        } catch (Exception e) {
            tr.rollback();
            addMessage(e.getMessage());
            e.printStackTrace();
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
        return map;
    }

    public void onAulaChange() {
        //
    }

    public Map<String, String> getAulas() {
        return aulas;
    }

    public void setAulas(Map<String, String> aulas) {
        this.aulas = aulas;
    }

    public String getStraula() {
        return straula;
    }

    public void setStraula(String straula) {
        this.straula = straula;
    }

    /**
     * Atualiza o Checkin negando-o. Usado em aprovacao.xhtml
     */
    public void swipeCheckinEsquerda(SwipeEvent event) {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            tr = session.beginTransaction();
            checkinSelecionado = (Checkin) event.getData();
            List<Checkin> lista = session.createQuery(
                    "select c  "
                    + "from Checkin c "
                    + "where c.usuario.email ='" + checkinSelecionado.getUsuario().getEmail() + "' and c.aula.idaula = '" + checkinSelecionado.getAula().getIdaula() + "'", Checkin.class)
                    .getResultList();
            if (!lista.isEmpty()) {
                Checkin checkin1 = new Checkin();
                checkin1 = (Checkin) lista.get(0);
                checkin1.setConfirma(Boolean.FALSE);
                checkin1.setUpdateTime(new Date());
                checkin1.setAprovadorConfirmador(AulasHelper.getNomeExibicao());
                session.saveOrUpdate(checkin1);
                checkin1 = new Checkin();

            }
            tr.commit();
            puneCheckinFake();
        } catch (Exception e) {
            if (tr != null && tr.isActive()) {
                try {
                    // Second try catch as the rollback could fail as well
                    tr.rollback();
                } catch (HibernateException e1) {
                    System.out.println("Erro no rollback do transaction");
                }
                // throw again the first exception
                throw e;
            }
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }

    }

    /**
     * Remove checkin quando reprovado negando-o.(Punição por checkin fake)
     * Usado em aprovacao.xhtml
     */
    public void puneCheckinFake() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        String str = "Reprovado. Perde um checkin";
        try {
            tr = session.beginTransaction();
            List<Checkin> lista2 = session.createQuery(
                    "select c  "
                    + "from Checkin c "
                    + "where c.usuario.email ='" + checkinSelecionado.getUsuario().getEmail() + "' and c.aula.categoriaaula.idcategoriaaula = '" + checkinSelecionado.getAula().getCategoriaaula().getIdcategoriaaula() + "' and c.confirma = 1", Checkin.class
            )
                    .setMaxResults(1).getResultList();
            if (!lista2.isEmpty()) {
                Checkin checkin1 = (Checkin) lista2.get(0);
                checkin1.setConfirma(Boolean.FALSE);
                checkin1.setUpdateTime(checkinSelecionado.getUpdateTime());
                session.saveOrUpdate(checkin1);
                checkin1 = new Checkin();
            }
            tr.commit();
            addMessage(str);
        } catch (Exception e) {
            if (tr != null && tr.isActive()) {
                try {
                    // Second try catch as the rollback could fail as well
                    tr.rollback();
                } catch (HibernateException e1) {
                    System.out.println("Erro no rollback do transaction");
                }
                // throw again the first exception
                throw e;
            }
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }

    /**
     * Atualiza o Checkin aprovando-o. Usado em aprovacao.xhtml
     */
    public void swipeCheckinDireita(SwipeEvent event) {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            tr = session.beginTransaction();
            Checkin checkin = (Checkin) event.getData();
            String str = "Aprovado!";
            List<Checkin> lista = session.createQuery(
                    "select c  "
                    + "from Checkin c "
                    + "where c.usuario.email ='" + checkin.getUsuario().getEmail() + "' and c.aula.idaula = '" + checkin.getAula().getIdaula() + "'", Checkin.class)
                    .getResultList();
            if (!lista.isEmpty()) {
                Checkin checkin1 = new Checkin();
                checkin1 = (Checkin) lista.get(0);
                checkin1.setConfirma(Boolean.TRUE);
                checkin1.setUpdateTime(new Date());
                checkin1.setAprovadorConfirmador(AulasHelper.getNomeExibicao());
                session.saveOrUpdate(checkin1);
                checkin1 = new Checkin();

            }
            tr.commit();
            addMessage(str);

        } catch (Exception e) {
            if (tr != null && tr.isActive()) {
                try {
                    // Second try catch as the rollback could fail as well
                    tr.rollback();
                } catch (HibernateException e1) {
                    System.out.println("Erro no rollback do transaction");
                }
                // throw again the first exception
                throw e;
            }
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }

    }

    /**
     * Atualiza o CheckinCompeticao negando-o. Usado em aprovacao.xhtml
     */
    public void swipeCheckinCompeticaoEsquerda(SwipeEvent event) {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        String str = "";
        try {
            tr = session.beginTransaction();
            Checkincompeticao checkin = (Checkincompeticao) event.getData();
            List<Checkincompeticao> lista = session.createQuery(
                    "select c  "
                    + "from Checkincompeticao c "
                    + "where c.usuario.email ='" + checkin.getUsuario().getEmail() + "' and c.idcheckincompeticao= '" + checkin.getIdcheckincompeticao() + "'", Checkincompeticao.class)
                    .getResultList();
            if (!lista.isEmpty()) {
                if (checkin.getTipo().matches("Staff")) {
                    Checkincompeticao checkin1 = new Checkincompeticao();
                    checkin1 = (Checkincompeticao) lista.get(0);
                    checkin1.setUpdateTime(new Date());
                    checkin1.setQtd(-2);
                    checkin1.setAprovadorConfirmador(AulasHelper.getNomeExibicao());
                    session.save(checkin1);
                    session.flush();
                    str="Reprovado perde 2 checkins.";
                }
                if (checkin.getTipo().matches("Competidor")) {
                    Checkincompeticao checkin1 = new Checkincompeticao();
                    checkin1 = (Checkincompeticao) lista.get(0);
                    checkin1.setUpdateTime(new Date());
                    checkin1.setQtd(-5);
                    checkin1.setAprovadorConfirmador(AulasHelper.getNomeExibicao());
                    session.save(checkin1);
                    session.flush();
                    str="Reprovado perde 5 checkins.";
                }
                if (checkin.getTipo().matches("Medalhista")) {
                    Checkincompeticao checkin1 = new Checkincompeticao();
                    checkin1 = (Checkincompeticao) lista.get(0);
                    checkin1.setUpdateTime(new Date());
                    checkin1.setQtd(-7);
                    checkin1.setAprovadorConfirmador(AulasHelper.getNomeExibicao());
                    session.save(checkin1);
                    session.flush();
                    str="Reprovado perde 7 checkins.";
                }
            }
            tr.commit();
            addMessage(str);
        } catch (Exception e) {
            if (tr != null && tr.isActive()) {
                try {
                    // Second try catch as the rollback could fail as well
                    tr.rollback();
                } catch (HibernateException e1) {
                    System.out.println("Erro no rollback do transaction");
                }
                // throw again the first exception
                throw e;
            }
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }


    /**
     * Atualiza o Checkin Competição aprovando-o. Usado em aprovacao.xhtml
     */
    public void swipeCheckinCompeticaoDireita(SwipeEvent event) {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            tr = session.beginTransaction();
            Checkincompeticao checkin = (Checkincompeticao) event.getData();
            String str = "Aprovado!";
            List<Checkincompeticao> lista = session.createQuery(
                    "select c  "
                    + "from Checkincompeticao c "
                    + "where c.usuario.email ='" + checkin.getUsuario().getEmail() + "' and c.idcheckincompeticao= '" + checkin.getIdcheckincompeticao() + "'", Checkincompeticao.class)
                    .getResultList();
            if (!lista.isEmpty()) {
                if (checkin.getTipo().matches("Staff")) {
                    Checkincompeticao checkin1 = new Checkincompeticao();
                    checkin1 = (Checkincompeticao) lista.get(0);
                    checkin1.setUpdateTime(new Date());
                    checkin1.setQtd(2);
                    checkin1.setAprovadorConfirmador(AulasHelper.getNomeExibicao());
                    session.save(checkin1);
                    session.flush();
                }
                if (checkin.getTipo().matches("Competidor")) {
                    Checkincompeticao checkin1 = new Checkincompeticao();
                    checkin1 = (Checkincompeticao) lista.get(0);
                    checkin1.setUpdateTime(new Date());
                    checkin1.setQtd(5);
                    checkin1.setAprovadorConfirmador(AulasHelper.getNomeExibicao());
                    session.save(checkin1);
                    session.flush();
                }
                if (checkin.getTipo().matches("Medalhista")) {
                    Checkincompeticao checkin1 = new Checkincompeticao();
                    checkin1 = (Checkincompeticao) lista.get(0);
                    checkin1.setUpdateTime(new Date());
                    checkin1.setQtd(7);
                    checkin1.setAprovadorConfirmador(AulasHelper.getNomeExibicao());
                    session.save(checkin1);
                    session.flush();
                }
            }
            tr.commit();
            addMessage(str);
        } catch (Exception e) {
            if (tr != null && tr.isActive()) {
                try {
                    // Second try catch as the rollback could fail as well
                    tr.rollback();
                } catch (HibernateException e1) {
                    System.out.println("Erro no rollback do transaction");
                }
                // throw again the first exception
                throw e;
            }
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }

    /**
     * Atualiza o Checkin Seminário negando-o. Usado em aprovacao.xhtml
     */
    public void swipeCheckinSeminarioEsquerda(SwipeEvent event) {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            tr = session.beginTransaction();
            Checkinseminario checkin = (Checkinseminario) event.getData();
            String str = "";
            List<Checkinseminario> lista = session.createQuery(
                    "select c  "
                    + "from Checkinseminario c "
                    + "where c.usuario.email ='" + checkin.getUsuario().getEmail() + "' and c.idcheckinseminario= '" + checkin.getIdcheckinseminario() + "'", Checkinseminario.class)
                    .getResultList();
            if (!lista.isEmpty()) {
                if (checkin.getTipo().matches("Preta")) {
                    Checkinseminario checkin1 = new Checkinseminario();
                    checkin1 = (Checkinseminario) lista.get(0);
                    checkin1.setUpdateTime(new Date());
                    checkin1.setQtd(-5);
                    checkin1.setAprovadorConfirmador(AulasHelper.getNomeExibicao());
                    session.saveOrUpdate(checkin1);
                    session.flush();
                    checkin1 = new Checkinseminario();
                    str="Reprovado perde 5 checkins.";
                }
                if (checkin.getTipo().matches("Behring")) {
                    Checkinseminario checkin1 = new Checkinseminario();
                    checkin1 = (Checkinseminario) lista.get(0);
                    checkin1.setUpdateTime(new Date());
                    checkin1.setQtd(-10);
                    checkin1.setAprovadorConfirmador(AulasHelper.getNomeExibicao());
                    session.saveOrUpdate(checkin1);
                    session.flush();
                    checkin1 = new Checkinseminario();
                    str="Reprovado perde 10 checkins.";
                }

            }
            tr.commit();
            addMessage(str);
        } catch (Exception e) {
            if (tr != null && tr.isActive()) {
                try {
                    // Second try catch as the rollback could fail as well
                    tr.rollback();
                } catch (HibernateException e1) {
                    System.out.println("Erro no rollback do transaction");
                }
                // throw again the first exception
                throw e;
            }
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }

    /**
     * Atualiza o Checkin Seminário aprovando-o. Usado em aprovacao.xhtml
     */
    public void swipeCheckinSeminarioDireita(SwipeEvent event) {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        try {
            tr = session.beginTransaction();
            Checkinseminario checkin = (Checkinseminario) event.getData();
            String str = "Aprovado!";
            List<Checkinseminario> lista = session.createQuery(
                    "select c  "
                    + "from Checkinseminario c "
                    + "where c.usuario.email ='" + checkin.getUsuario().getEmail() + "' and c.idcheckinseminario= '" + checkin.getIdcheckinseminario() + "'", Checkinseminario.class)
                    .getResultList();
            if (!lista.isEmpty()) {
                if (checkin.getTipo().matches("Preta")) {
                    Checkinseminario checkin1 = new Checkinseminario();
                    checkin1 = (Checkinseminario) lista.get(0);
                    checkin1.setUpdateTime(new Date());
                    checkin1.setQtd(5);
                    checkin1.setAprovadorConfirmador(AulasHelper.getNomeExibicao());
                    session.save(checkin1);
                    session.flush();
                    checkin1 = new Checkinseminario();
                }
                if (checkin.getTipo().matches("Behring")) {
                    Checkinseminario checkin1 = new Checkinseminario();
                    checkin1 = (Checkinseminario) lista.get(0);
                    checkin1.setUpdateTime(new Date());
                    checkin1.setQtd(10);
                    checkin1.setAprovadorConfirmador(AulasHelper.getNomeExibicao());
                    session.save(checkin1);
                    session.flush();
                    checkin1 = new Checkinseminario();
                }

            }
            tr.commit();
            addMessage(str);
        } catch (Exception e) {
            if (tr != null && tr.isActive()) {
                try {
                    // Second try catch as the rollback could fail as well
                    tr.rollback();
                } catch (HibernateException e1) {
                    System.out.println("Erro no rollback do transaction");
                }
                // throw again the first exception
                throw e;
            }
        } finally {
            if (session != null) {
                try {
                    session.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }

    public void addMessage(String summary) {
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary, null);
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * Atualiza o aprovador conforme o usuarioMap escolhido em addAprovacao em
     * admin.xhtml
     */
    public void onAulaChangeAprovador() {
        Transaction tr = null;
        Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        if ((!straula.isEmpty()) && !strusuario.isEmpty()) {
            try {
                tr = session.beginTransaction();
                List<Aula> listaAula = session.createQuery(
                        "select a  "
                        + "from Aula a where a.idaula ='" + Integer.parseInt(straula) + "'", Aula.class)
                        .getResultList();
                List<Usuario> listaUsuario = session.createQuery(
                        "select u  "
                        + "from Usuario u where u.email = '" + strusuario + "'", Usuario.class)
                        .getResultList();
                if (!listaAula.isEmpty() && !listaUsuario.isEmpty()) {
                    Aprovador aprovador = new Aprovador();
                    Aula aula = new Aula();
                    Usuario usuario = new Usuario();
                    aula = (Aula) listaAula.get(0);
                    usuario = (Usuario) listaUsuario.get(0);
                    aprovador.setAula(aula);
                    aprovador.setUsuario(usuario);
                    aprovador.setAulaIdaula(aula.getIdaula());
                    aprovador.setAula(aula);
                    session.merge(aprovador);
                    aprovador = new Aprovador();
                }
                tr.commit();
                addMessage("Alterado");
                strusuario = "";
            } catch (Exception e) {
                if (tr != null && tr.isActive()) {
                    try {
                        // Second try catch as the rollback could fail as well
                        tr.rollback();
                    } catch (HibernateException e1) {
                        System.out.println("Erro no rollback do transaction");
                    }
                    // throw again the first exception
                    throw e;
                }
            } finally {
                if (session != null) {
                    try {
                        session.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            }
        }

    }

    public String getStrusuario() {
        return strusuario;
    }

    public void setaulasRecentesAprovar(String strusuario) {
        this.strusuario = strusuario;
    }

    public void setStrusuario(String strusuario) {
        this.strusuario = strusuario;
    }

    public Checkin getCheckinSelecionado() {
        return checkinSelecionado;
    }

    public void setCheckinSelecionado(Checkin checkinSelecionado) {
        this.checkinSelecionado = checkinSelecionado;
    }

}
