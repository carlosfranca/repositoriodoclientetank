package oAuth;

import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.Version;
import com.restfb.WebRequestor.Response;
import com.restfb.json.JsonArray;
import com.restfb.json.JsonObject;
import helpers.AulasHelper;
import java.util.List;
import javax.ejb.Stateless;
import javax.faces.context.FacesContext;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import system.HibernateUtil;

/**
 *
 * @author carlos
 */
public class UsuarioService {

    public static String struser, stremail, strpicture = "";

    public void authFacebookLogin(String accessToken, int expires) {
        try {
//            JSONObject resp = new JSONObject(
//                    new URL("https://graph.facebook.com/me?access_token=" + accessToken));
//            id = resp.getString("id");
//            nome = resp.getString("first_name");
//            sobrenome = resp.getString("last_name");
//            email = resp.getString("email");

            FacebookClient fb = new DefaultFacebookClient(accessToken, Version.LATEST);

            oAuth.Usuario user = fb.fetchObject("me", oAuth.Usuario.class, Parameter.with("fields", "email,name"));
            struser = user.getName();
            stremail = user.getEmail();
            JsonObject picture
                    = fb.fetchObject("/me/picture", JsonObject.class,
                            Parameter.with("width", "70"), // the image size
                            Parameter.with("redirect", "false")); // don't redirect
            JsonObject picture2 = (JsonObject) picture.get("data");
            strpicture = picture2.get("url").toString();

        } catch (Throwable ex) {
            throw new RuntimeException("failed login", ex);
        }
    }

    public static String getStruser() {
        return struser;
    }

    public static void setStruser(String struser) {
        UsuarioService.struser = struser;
    }

    public static String getStremail() {
        return stremail;
    }

    public static void setStremail(String stremail) {
        UsuarioService.stremail = stremail;
    }

    public static String getStrpicture() {
        return strpicture;
    }

    public static void setStrpicture(String strpicture) {
        UsuarioService.strpicture = strpicture;
    }

}
