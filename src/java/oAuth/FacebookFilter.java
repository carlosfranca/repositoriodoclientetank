package oAuth;

import com.mysql.jdbc.StringUtils;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static oAuth.UsuarioService.stremail;

/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
/**
 *
 * @author dimmyk
 */
public class FacebookFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest sr, ServletResponse sr1, FilterChain fc) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) sr;
        HttpServletResponse res = (HttpServletResponse) sr1;
        String code = sr.getParameter("code");

        if (!StringUtils.isNullOrEmpty(code)) {
            String authURL = FaceBookClient.getAuthURL(code);
            URL url = new URL(authURL);
            try {
                String result = leURL(url);
                String accessToken = null;
                Integer expires = null;
                String[] pairs = result.split("&");
                for (String pair : pairs) {
                    String[] kv = pair.split("=");
                    if (kv.length != 2) {
                        throw new RuntimeException("Unexpected auth response");
                    } else {
                        if (kv[0].equals("access_token")) {
                            accessToken = kv[1];
                        }
                        if (kv[0].equals("expires")) {
                            expires = Integer.valueOf(kv[1]);
                        }
                    }
                }
                if (accessToken != null && expires != null) {

                    Usuario u = new Usuario();
                    UsuarioService us = new UsuarioService();
                    us.authFacebookLogin(accessToken, expires);
                    if (stremail == null || stremail == "") {
                        //res.sendRedirect("http://localhost:8080/checkin/faces/faltaEmail.xhtml");//teste
                        res.sendRedirect("http://wildflydev-teamfabiaoapp.rhcloud.com/checkin/faces/faltaEmail.xhtml");
                    } else {
                        //res.sendRedirect("http://localhost:8080/checkin/faces/arealogada.xhtml");//teste
                        res.sendRedirect("http://wildflydev-teamfabiaoapp.rhcloud.com/checkin/faces/arealogada.xhtml");
                    }
                } else {
                    throw new RuntimeException("Access token and expires not found");
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } else {
            res.sendRedirect(FaceBookClient.getLoginRedirectURL());
        }
    }

    private String readURL(URL url) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        InputStream is = url.openStream();
        int r;
        while ((r = is.read()) != -1) {
            baos.write(r);
        }
        return new String(baos.toByteArray());
    }

    public String leURL(URL url) throws IOException {
        return readURL(url);
    }

    @Override
    public void destroy() {
    }
}
