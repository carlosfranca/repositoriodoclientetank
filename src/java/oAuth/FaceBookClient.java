/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package oAuth;

/**
 *
 * @author dimmyk
 */

public class FaceBookClient {

    //public static String API_KEY = "266350383735222";//teste
    public static String API_KEY = "260933157610278";
    //public static String SECRET = "7bb667cee1608c11d69b61c741254293";//teste
    public static String SECRET = "7e225f1fe07faf5f7eaa03043a03a263";
    //private static final String client_id = "266350383735222";//teste
    private static final String client_id = "260933157610278";
    // set this to your servlet URL for the authentication servlet/filter
    //private static final String redirect_uri = "http://localhost:8080/checkin/faces/validaLogin.html";//teste
    private static final String redirect_uri = "http://wildflydev-teamfabiaoapp.rhcloud.com/checkin/faces/validaLogin.html";
    /// set this to the list of extended permissions you want
    private static final String[] perms = new String[]{"email"};

    public static String getAPIKey() {
        return API_KEY;
    }

    public static String getSecret() {
        return SECRET;
    }

    public static String getLoginRedirectURL() {
        return "https://graph.facebook.com/oauth/authorize?client_id="
                + API_KEY + "&display=page&redirect_uri="
                + redirect_uri + "&scope=email";
    }

    public static String getAuthURL(String authCode) {
        return "https://graph.facebook.com/oauth/access_token?client_id="
                + API_KEY + "&redirect_uri="
                + redirect_uri + "&client_secret=" + SECRET + "&code=" + authCode;
    }
}
