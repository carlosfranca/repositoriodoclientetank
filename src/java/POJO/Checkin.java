package POJO;
// Generated 17/08/2016 23:39:57 by Hibernate Tools 4.3.1


import java.util.Date;

/**
 * Checkin generated by hbm2java
 */
public class Checkin  implements java.io.Serializable {


     private CheckinId id;
     private Aula aula;
     private Usuario usuario;
     private Date createTime;
     private Date updateTime;
     private Boolean cria;
     private Boolean confirma;
     private String aprovadorConfirmador;

    public Checkin() {
    }

	
    public Checkin(CheckinId id, Aula aula, Usuario usuario) {
        this.id = id;
        this.aula = aula;
        this.usuario = usuario;
    }
    public Checkin(CheckinId id, Aula aula, Usuario usuario, Date createTime, Date updateTime, Boolean cria, Boolean confirma, String aprovadorConfirmador) {
       this.id = id;
       this.aula = aula;
       this.usuario = usuario;
       this.createTime = createTime;
       this.updateTime = updateTime;
       this.cria = cria;
       this.confirma = confirma;
       this.aprovadorConfirmador = aprovadorConfirmador;
    }
   
    public CheckinId getId() {
        return this.id;
    }
    
    public void setId(CheckinId id) {
        this.id = id;
    }
    public Aula getAula() {
        return this.aula;
    }
    
    public void setAula(Aula aula) {
        this.aula = aula;
    }
    public Usuario getUsuario() {
        return this.usuario;
    }
    
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    public Date getCreateTime() {
        return this.createTime;
    }
    
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
    public Date getUpdateTime() {
        return this.updateTime;
    }
    
    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
    public Boolean getCria() {
        return this.cria;
    }
    
    public void setCria(Boolean cria) {
        this.cria = cria;
    }
    public Boolean getConfirma() {
        return this.confirma;
    }
    
    public void setConfirma(Boolean confirma) {
        this.confirma = confirma;
    }
    public String getAprovadorConfirmador() {
        return this.aprovadorConfirmador;
    }
    
    public void setAprovadorConfirmador(String aprovadorConfirmador) {
        this.aprovadorConfirmador = aprovadorConfirmador;
    }




}


