/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package system;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtil {

    private static SessionFactory sessionFactory;

//maling the Hibernate SessionFactory object as singleton
    public synchronized static SessionFactory getSessionFactory() {

        if (sessionFactory == null) {
            try {
                StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder()
                        .configure("hibernate.cfg.xml")
                        .build();

                Metadata metadata = new MetadataSources(standardRegistry)
                        .getMetadataBuilder()
                        .build();
                sessionFactory = (SessionFactory) metadata.getSessionFactoryBuilder().build();
            } catch (HibernateException he) {
                System.err.println("Error creating Session: " + he);
                throw new ExceptionInInitializerError(he);
            }
        }
        return sessionFactory;
    }

    public  static void closeSessionFactory() {
        if (sessionFactory != null) {
            sessionFactory.close();
        }
    }

}
